import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from adanowo_simulator.datatypes import StateDict, ConstraintsDict, OutputDict, OutputArray, StateArray


def array_to_dict(array: StateArray, keys: list[str],
                  bounds_for_scaling: ConstraintsDict | None = None, mode="min_max") -> StateDict:
    """
    This method should be used to convert an action array to a dictionary. This avoids mixups in the order of the
    values. The keys are used to assign the values to the correct keys.
    """

    if array.size != len(keys):
        raise ValueError("Length of array and keys are not the same.")
    dictionary = dict()
    for index, key in enumerate(keys):
        if bounds_for_scaling is None:
            dictionary[key] = float(array[index].flatten()[0])
            continue
        upper_bound = float(bounds_for_scaling[key]["upper"])
        lower_bound = float(bounds_for_scaling[key]["lower"])
        if mode == "min_max":
            scaled_value = inverse_min_max_normalization(array[index], lower_bound, upper_bound)
        elif mode == "tanh":
            scaled_value = tanh_scale(array[index], lower_bound, upper_bound)
        elif mode == "div_by_range":
            scaled_value = mult_by_range(array[index], lower_bound, upper_bound)
        elif mode == "no_transformation":
            scaled_value = array[index]
        else:
            raise ValueError("Unknown mode.")
        dictionary[key] = float(scaled_value.flatten()[0])
    return dictionary


def dict_to_array(dictionary: OutputDict, keys: list[str],
                  bounds_for_scaling: ConstraintsDict | None = None, mode="min_max") -> OutputArray:
    """
    This method should be used to convert a dictionary to an action array.
    It serves as the inverse to the array_to_dict function.
    """
    if not set(keys).issubset(dictionary.keys()):
        raise ValueError("Not all keys from the list are contained in the state or output Dict.")
    array = np.zeros(len(keys))
    for index, key in enumerate(keys):
        if dictionary[key] is None:
            array[index] = np.nan
            continue
        if bounds_for_scaling is None:
            array[index] = dictionary[key]
            continue
        upper_bound = bounds_for_scaling[key]["upper"]
        lower_bound = bounds_for_scaling[key]["lower"]
        if mode == "min_max":
            scaled_value = min_max_normalization(np.array(dictionary[key]), lower_bound, upper_bound)
        elif mode == "tanh":
            scaled_value = inverse_tanh_scale(np.array(dictionary[key]), lower_bound, upper_bound)
        elif mode == "no_transformation":
            scaled_value = dictionary[key]
        else:
            raise ValueError("Unknown mode.")
        array[index] = scaled_value
    return array


def tanh_scale(array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
        Apply a tanh transformation to an array and scale the output to a specified range.
        This ensures that all agent actions are centered around 0 and have the same scale.
        Constraint violations are prevented by the tanh function.

        This method first applies the hyperbolic tangent (tanh) function to each element
        of the input array, which maps the values to the range (-1, 1). Then, it scales
        these values to a specified range (min_val, max_val).

        Parameters:
        array (np.array): The input array containing the values to be transformed.
        min_val (float): The minimum value of the desired output range.
        max_val (float): The maximum value of the desired output range.

        Returns:
        np.array: An array where each input value has been scaled to the specified range.

        Note:
        The input array should have values within the range that the tanh function can handle.
        Extremely large values may lead to numerical instability due to the properties of tanh.
        """
    # Apply the tanh transformation
    tanh_array = np.tanh(array)
    # Scale to the desired range
    scaled_array = min_val + (0.5 * (tanh_array + 1) * (max_val - min_val))
    return scaled_array


def inverse_tanh_scale(scaled_array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
    Applies the inverse hyperbolic tangent scaling to a scaled array, reverting it to its
    original values before tanh scaling was applied.

    This function first linearly scales the input array from its current scale (defined by `min_val`
    and `max_val`) to a (0, 1) range. Then, it adjusts this range to (-1, 1) before applying the
    inverse hyperbolic tangent function (arctanh) to revert the tanh scaling. The process is useful
    for decoding data that was previously transformed using a tanh function to fit within a specific
    range.

    Parameters:
    - scaled_array (np.array): The array that has been scaled and needs to be reverted.
    - min_val (float): The minimum value of the original scale before tanh scaling was applied.
    - max_val (float): The maximum value of the original scale before tanh scaling was applied.

    Returns:
    - np.array: The array reverted to its original scale before any tanh scaling was applied.

    Raises:
    - ValueError: If any value in `adjusted_array` is outside the domain of the arctanh function,
      specifically not in the open interval (-1, 1).

    Note:
    - Extreme caution should be used when dealing with the arctanh function due to its singularity
      at -1 and 1. Ensure that the input array strictly adheres to the expected preconditions to
      avoid undefined behavior or errors.
    """
    # Invert the scaling to get values in the range of (0, 1)
    normalized_array = (scaled_array - min_val) / (max_val - min_val)
    # Scale from (0, 1) to (-1, 1)
    adjusted_array = 2 * normalized_array - 1
    # Apply the inverse tanh (atanh)
    return np.arctanh(adjusted_array)


def inverse_min_max_normalization(array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
    Revert min-max normalization on an array, scaling its values back to their original range.

    This function applies the inverse of the min-max normalization process to transform the
    normalized array values back to their original scale. It uses the formula:
    min_val + (0.5 * (x + 1) * (max_val - min_val)), where `x` is an element in the normalized array.
    The function uses `min_val` and `max_val` which are the original minimum and maximum values
    of the data before it was normalized.

    Parameters:
    - array (np.array): The input array to be inversely normalized. It is expected to contain
      values scaled between -1 and 1.
    - min_val (float): The original minimum value of the data before normalization.
    - max_val (float): The original maximum value of the data before normalization.

    Returns:
    - np.array: The array with its values scaled back to the original range.
    """
    scaled_array = min_val + (0.5 * (array + 1) * (max_val - min_val))
    return scaled_array


def min_max_normalization(array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
    Perform min-max normalization on an array to scale its values to a specified range.

    This function scales the input array values to a range between -1 and 1 using the
    formula: -1 + 2 * (x - min_val) / (max_val - min_val), where `x` is an element in the array.
    The function expects `min_val` and `max_val` to be the minimum and maximum values of the
    desired scale range, respectively.

    Parameters:
    - array (np.array): The input array to be normalized.
    - min_val (float): The minimum value used for scaling, typically the minimum value found in the array.
    - max_val (float): The maximum value used for scaling, typically the maximum value found in the array.

    Returns:
    - np.array: The normalized array with values scaled to the range [-1, 1].
    """
    normalized_array = -1 + 2*(array - min_val) / (max_val - min_val)
    return normalized_array


def mult_by_range(array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
    Apply a division by range scaling.This ensures that all agent actions have the same scale,
    but they are not centered around 0.

    Parameters:
    - array (np.array): The input array containing the values to be transformed.
    - min_val (float): The minimum value of the desired output range.
    - max_val (float): The maximum value of the desired output range.

    Returns:
    - np.array: An array where each input value has been scaled to the specified range.
    """
    # Calculate the range of the values
    range_val = max_val - min_val
    # Apply the division by range scaling
    scaled_array = array * range_val/2
    return scaled_array


def div_by_range(scaled_array: StateArray, min_val: float, max_val: float) -> StateArray:
    """
    Apply an inverse of the division by range scaling. This reverses the scaling applied
    by the `mult_by_range` function to return the array to its original scale.

    Parameters:
    - scaled_array (np.array): The input array containing the scaled values to be transformed.
    - min_val (float): The minimum value of the original range used in `mult_by_range`.
    - max_val (float): The maximum value of the original range used in `mult_by_range`.

    Returns:
    - np.array: An array where each scaled value has been transformed back to the original scale.
    """
    # Calculate the range of the values
    range_val = max_val - min_val
    # Apply the inverse of the division by range scaling
    original_array = scaled_array / (range_val / 2)
    return original_array


class ParameterizedStandardScaler(BaseEstimator, TransformerMixin):
    def __init__(self, mean: float, std: float):
        self.mean = mean
        self.std = std

    def fit(self, X: float, y=None):
        # In this custom implementation, fit does nothing but return itself.
        return self

    def transform(self, X: float, y=None) -> float:
        # Apply the transformation: (X - mean) / std
        return (X - self.mean) / self.std

    def inverse_transform(self, X: float, y=None) -> float:
        # Reverse the transformation: X * std + mean
        return X * self.std + self.mean
