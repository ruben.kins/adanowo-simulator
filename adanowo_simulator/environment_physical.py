import sys
import logging
from copy import copy
from omegaconf import DictConfig

from adanowo_simulator.abstract_base_classes.output_manager import AbstractOutputManager
from adanowo_simulator.abstract_base_classes.objective_manager import AbstractObjectiveManager
from adanowo_simulator.abstract_base_classes.action_manager import AbstractActionManager
from adanowo_simulator.abstract_base_classes.disturbance_manager import AbstractDisturbanceManager
from adanowo_simulator.abstract_base_classes.experiment_tracker import AbstractExperimentTracker
from adanowo_simulator.abstract_base_classes.scenario_manager import AbstractScenarioManager
from adanowo_simulator.datatypes import StateDict, OutputDict, OptionalConstraintsDict
from adanowo_simulator.environment import Environment, compile_log_variables

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


class RealisticEnvironment(Environment):
    def __init__(
            self, config: DictConfig, disturbance_manager: AbstractDisturbanceManager,
            action_manager: AbstractActionManager, output_manager: AbstractOutputManager,
            objective_manager: AbstractObjectiveManager, scenario_manager: AbstractScenarioManager,
            experiment_tracker: AbstractExperimentTracker):
        super().__init__(config, disturbance_manager, action_manager, output_manager, objective_manager,
                         scenario_manager, experiment_tracker)
        self.warmstart = False
        self._safety_violation = False

    def step(self, actions: dict) -> tuple[float, StateDict, OutputDict, OptionalConstraintsDict]:
        if not self._ready:
            raise RuntimeError("Cannot call step() before calling reset().")
        try:
            if self._step_index == 1:
                logger.info("Experiment is running.")
            assert set(actions.keys()) == set(self._config.used_setpoints), "Action dict does not match used setpoints."
            # get disturbances in case they are needed for validity checks:
            disturbances = self._disturbance_manager.step()
            # check if action is valid, but do not calculate dependent, as they might change with the output manager:
            setpoints, _, tentative_dependent, setpoints_okay, dependent_variables_okay = \
                self._action_manager.step(actions, disturbances)
            outputs = {key: None for key in list(self._config.used_outputs)}

            self._check_safety_violation(setpoints_okay, dependent_variables_okay, outputs)

            if self._safety_violation:
                f_aug = None
                f_obj = None
                dependent_variables = tentative_dependent
                state = disturbances | copy(actions) | dependent_variables
                _, _, output_constraints_met = self._objective_manager.step(
                    state, outputs, self._safety_violation
                )
                # Execute scenario for the next step so the agent is already informed about production context changes.
                state_with_new_context, quality_bounds_next = self._prepare_next_step(
                    copy(actions), dependent_variables
                )
            else:  # only perform expensive experiment if action is valid:
                temp_state = disturbances | setpoints | tentative_dependent
                outputs: OutputDict = self._output_manager.step(temp_state)
                # Update setpoints and disturbances in case the state got changed by the output manager:
                for dkey in disturbances.keys():
                    disturbances[dkey] = temp_state[dkey]
                    self._disturbance_manager.config.disturbances[dkey] = temp_state[dkey]
                for skey in setpoints.keys():
                    setpoints[skey] = temp_state[skey]
                # now calculate dependent variables after the state has been updated:
                _, dependent_variables, _, _, _ = self._action_manager.step(setpoints, disturbances)
                state = disturbances | setpoints | dependent_variables

                f_aug, f_obj, output_constraints_met = self._objective_manager.step(
                    state, outputs, self._safety_violation
                )
                self._check_safety_violation(setpoints_okay, dependent_variables_okay, outputs,
                                             overwrite_when_valid=False)
                # Execute scenario for the next step so the agent is already informed about production context changes.
                state_with_new_context, quality_bounds_next = self._prepare_next_step(setpoints, dependent_variables)
            log_variables = compile_log_variables(
                f_aug,
                setpoints_okay,
                dependent_variables_okay,
                output_constraints_met,
                actions,
                outputs,
                setpoints,
                dependent_variables,
                disturbances
            )
            self._experiment_tracker.step(log_variables, self._step_index)
            self.log_vars = copy(log_variables)

        except Exception as e:
            self.close()
            raise e

        if self._return_penalty:
            objective_value = f_aug
        else:
            objective_value = f_obj

        return objective_value, state_with_new_context, outputs, quality_bounds_next

    def reset(self) -> tuple[float, StateDict, OutputDict, OptionalConstraintsDict]:
        logger.info("Resetting environment...")
        try:
            # step 0.
            self._step_index = 0
            self._config = self._initial_config.copy()
            self._scenario_manager.reset()
            # get disturbances in case they are needed for validity checks:
            disturbances = self._disturbance_manager.reset()

            setpoints, dependent_variables, _, setpoints_okay, dependent_variables_okay = \
                self._action_manager.reset(disturbances)

            temp_full_state = disturbances | setpoints | dependent_variables

            if not self.warmstart:
                outputs = self._output_manager.reset(temp_full_state)
                # Update setpoints and disturbances in case the state got changed by the output manager:
                for dkey in disturbances.keys():
                    disturbances[dkey] = temp_full_state[dkey]
                    self._disturbance_manager.config.disturbances[dkey] = temp_full_state[dkey]
                for skey in setpoints.keys():
                    setpoints[skey] = temp_full_state[skey]
                # now calculate dependent variables after the state has been updated:
                _, dependent_variables, _, _, _ = self._action_manager.step(setpoints, disturbances)

                state = disturbances | setpoints | dependent_variables
                f_aug, f_obj, output_constraints_met = self._objective_manager.reset(state, outputs, False)
            else:
                outputs = {key: 0 for key in list(self._config.used_outputs)}
                f_aug, f_obj = None, None
                output_constraints_met = {key: False for key in list(self._config.used_outputs)}
                _ = self.output_manager.reset(temp_full_state, no_interaction=True)
                _, _, _ = self._objective_manager.reset(temp_full_state, outputs, False)
            self._check_safety_violation(setpoints_okay, dependent_variables_okay, outputs, overwrite_when_valid=False)

            log_variables = compile_log_variables(
                f_aug,
                setpoints_okay,
                dependent_variables_okay,
                output_constraints_met,
                {},
                outputs,
                setpoints,
                dependent_variables,
                disturbances
            )
            self._experiment_tracker.reset(log_variables)
            self.log_vars = copy(log_variables)

            # prepare step 1.
            state_with_new_context, quality_bounds_next = self._prepare_next_step(setpoints, dependent_variables)

        except Exception as e:
            self.close()
            raise e

        self._ready = True
        logger.info("...environment has been reset.")

        if self._return_penalty:
            objective_value = f_aug
        else:
            objective_value = f_obj

        return objective_value, state_with_new_context, outputs, quality_bounds_next

