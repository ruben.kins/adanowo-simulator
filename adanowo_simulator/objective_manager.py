from typing import Callable
from omegaconf import DictConfig, OmegaConf

from adanowo_simulator.abstract_base_classes.objective_manager import AbstractObjectiveManager
from adanowo_simulator.datatypes import StateDict, OutputDict, ConstraintCheckDict


class ObjectiveManager(AbstractObjectiveManager):
    def __init__(self, objective_function: Callable, penalty_function: Callable, config: DictConfig):
        self._initial_config: DictConfig = config.copy()
        self._config: DictConfig = self._initial_config.copy()
        self._reward_function = objective_function
        self._penalty_function = penalty_function
        self._ready: bool = False

    @property
    def config(self) -> DictConfig:
        return self._config

    @config.setter
    def config(self, c):
        self._config = c

    def step(self, state: StateDict, outputs: OutputDict, safety_violation: bool) -> tuple[float, float,
                                                                                           ConstraintCheckDict]:
        if self._ready:
            try:
                outputs_okay = self._output_constraints_satisfied(outputs)
            except KeyError:
                raise KeyError("There has been a mismatch between outputs and constraints. Please check the config.")
            if safety_violation or (not (all(outputs_okay.values()))):
                f_aug = self._get_penalty(state, outputs)  # penalty
            else:
                f_aug = self._get_reward(state, outputs)  # no penalty
            f_obj = self._get_reward(state, outputs)  # f_obj only considers rewards
        else:
            raise RuntimeError("Cannot call step() before calling reset().")
        return f_aug, f_obj, outputs_okay

    def reset(self, initial_state: StateDict, initial_outputs: OutputDict,
              safety_violation_initially: bool) -> tuple[float, float, ConstraintCheckDict]:
        self._config = self._initial_config.copy()
        self._ready = True
        f_aug, f_obj, output_constraints_met = self.step(
            initial_state, initial_outputs, safety_violation_initially)
        return f_aug, f_obj, output_constraints_met

    def close(self) -> None:
        self._ready = False

    def _get_reward(self, state: StateDict, outputs: OutputDict) -> float:
        reward = self._reward_function(state, outputs, self._config.reward_parameters)
        return reward

    def _get_penalty(self, state: StateDict, outputs: OutputDict) -> float:
        penalty = self._penalty_function(state, outputs, self._config.reward_parameters)
        return penalty

    def _output_constraints_satisfied(self, outputs: OutputDict) -> ConstraintCheckDict:
        output_constraints_met = dict()
        for output_name, boundaries in self._config.output_bounds.items():
            for boundary_type in ["lower", "upper"]:
                boundary_value = boundaries.get(boundary_type)
                if boundary_value is None:
                    continue
                output = outputs[output_name]
                if output is None:
                    # if output is None, then it is not available. Then we default to not assuming it is violated.
                    output_constraints_met[f"{output_name}.{boundary_type}"] = True
                    continue
                comparison = output < boundary_value if boundary_type == "lower" else output > boundary_value
                output_constraints_met[f"{output_name}.{boundary_type}"] = not comparison

        return output_constraints_met
