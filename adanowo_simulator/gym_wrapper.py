import numpy as np
from omegaconf import OmegaConf
from gymnasium import Env, spaces
from gymnasium.core import RenderFrame
from omegaconf import DictConfig

from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
from adanowo_simulator import transformations
from adanowo_simulator.datatypes import StateArray, OutputArray, StateDict, OutputDict, OptionalConstraintsDict


class GymWrapper(Env):

    def __init__(self, environment: AbstractEnvironment, config: DictConfig, action_config: DictConfig,
                 env_config: DictConfig):
        self._environment: AbstractEnvironment = environment
        self._config: DictConfig = config.copy()
        self._action_config = action_config.copy()
        self._env_config = env_config
    
        self.action_space: spaces.Box = spaces.Box(low=-1, high=1, shape=(len(self._env_config.used_setpoints),),
                                                   dtype=np.float32)
        if not self._config.return_process_outputs:
            self.observation_space: spaces.Box = spaces.Box(
                low=-1, high=1, shape=(
                    len(self._env_config.used_setpoints)
                    + len(self._env_config.used_disturbances)
                    + len(self._env_config.used_dependent_variables),
                ), dtype=np.float32)
        else:
            self.observation_space: spaces.Box = spaces.Box(
                low=-1, high=1, shape=(
                    len(self._env_config.used_setpoints)
                    + len(self._env_config.used_disturbances)
                    + len(self._env_config.used_dependent_variables)
                    + len(self._env_config.used_outputs) * 3,  # output + 2 quality bounds
                    ), dtype=np.float32)

    @property
    def environment(self) -> AbstractEnvironment:
        return self._environment

    def step(self, actions_array: StateArray) -> tuple[OutputArray, float, bool, bool, dict]:
        # transform actions
        setpoint_keys = list(self._env_config.used_setpoints)
        scales = OmegaConf.to_container(self._action_config.setpoint_bounds)
        if self._action_config.actions_are_relative:
            mode = "div_by_range"
        else:
            mode = "min_max"
        action = transformations.array_to_dict(actions_array, setpoint_keys, bounds_for_scaling=scales, mode=mode)
        # perform step. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.step(action)

        # post-processing of env returns
        observations = self._compile_observations(state, outputs, quality_bounds)
        if self._config.scale_rewards:
            reward = reward / self._config.max_reward
        return observations, reward, False, False, dict()

    def reset(self, seed=None, options=None) -> tuple[StateArray, dict]:
        super().reset(seed=seed)
        # perform reset. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.reset()
        observations = self._compile_observations(state, outputs, quality_bounds)
        return observations, dict()

    def render(self) -> RenderFrame | list[RenderFrame] | None:
        pass

    def close(self) -> None:
        self._environment.close()

    def _compile_observations(self, state: StateDict, outputs: OutputDict,
                              quality_bounds: OptionalConstraintsDict) -> OutputArray:

        # tranform state variables
        setpoint_list = list(self._env_config.used_setpoints)
        setpoint_states = {key: state[key] for key in setpoint_list}
        bounds = OmegaConf.to_container(self._action_config.setpoint_bounds)
        setpoint_observations = transformations.dict_to_array(setpoint_states, setpoint_list, bounds, mode="min_max")
        
        # transform disturbances
        disturbance_list = list(self._env_config.used_disturbances)
        disturbance_states = {key: state[key] for key in disturbance_list}
        disturbance_bounds = OmegaConf.to_container(self._config.disturbance_bounds)
        disturbance_observations = transformations.dict_to_array(disturbance_states, disturbance_list,
                                                                 disturbance_bounds, mode="min_max")

        # transform dependent variables
        dependent_list = list(self._env_config.used_dependent_variables)
        dependent_states = {key: state[key] for key in dependent_list}
        dependent_bounds = OmegaConf.to_container(self._action_config.dependent_variable_bounds)
        dependent_variable_observations = transformations.dict_to_array(dependent_states, dependent_list,
                                                                        dependent_bounds, mode="min_max")

        if not self._config.return_process_outputs:  # do not concatenate process outputs
            observations = np.concatenate(
                (
                    setpoint_observations,
                    disturbance_observations,
                    dependent_variable_observations
                ), dtype=np.float32
            )
            return observations
        else:
            output_keys = list(self._env_config.used_outputs)
            output_bounds = OmegaConf.to_container(self._config.output_bounds)
            output_observations = transformations.dict_to_array(outputs, output_keys, output_bounds, mode="min_max")
            # set all NaN values to -1
            output_observations = np.nan_to_num(output_observations, nan=-1.0)
            
            # seperate lower and upper array for quality bounds
            for key in output_keys:
                if key not in quality_bounds.keys():
                    quality_bounds[key] = {"lower": None, "upper": None}
                if "lower" not in quality_bounds[key].keys():
                    quality_bounds[key]["lower"] = None
                if "upper" not in quality_bounds[key].keys():
                    quality_bounds[key]["upper"] = None
                
            quality_lower = {key: quality_bounds[key]["lower"] for key in output_keys}
            quality_upper = {key: quality_bounds[key]["upper"] for key in output_keys}

            quality_lower_array = transformations.dict_to_array(quality_lower, 
                                                                output_keys, 
                                                                output_bounds, 
                                                                mode="min_max")
            quality_upper_array = transformations.dict_to_array(quality_upper, 
                                                                output_keys, 
                                                                output_bounds, 
                                                                mode="min_max")

            quality_lower_array = np.nan_to_num(quality_lower_array, nan=-1.0)
            quality_upper_array = np.nan_to_num(quality_upper_array, nan=1.0)
            
            observations = np.concatenate(
                (
                    setpoint_observations,
                    disturbance_observations,
                    dependent_variable_observations,
                    output_observations,
                    quality_lower_array,
                    quality_upper_array
                ), dtype=np.float32
            )
        return observations
