from omegaconf import DictConfig, OmegaConf

from adanowo_simulator.objective_manager import ObjectiveManager
from adanowo_simulator.action_manager import ActionManager
from adanowo_simulator.disturbance_manager import DisturbanceManager
from adanowo_simulator.output_manager import ParallelOutputManager, SequentialOutputManager
from adanowo_simulator.output_manager_opcua import OpcuaOutputManager
from adanowo_simulator.scenario_manager import ScenarioManager
from adanowo_simulator.experiment_tracker import WandBTracker, EmptyTracker
from adanowo_simulator.environment import Environment
from adanowo_simulator.environment_physical import RealisticEnvironment
from adanowo_simulator.objective_functions import baseline_objective, baseline_penalty
from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
import adanowo_simulator.objective_functions_augsburg as objective_functions_augsburg


class EnvironmentFactory:
    def __init__(self, config: DictConfig):
        self.config = config.copy()

    def create_disturbance_manager(self):
        try:
            return DisturbanceManager(self.config.disturbance_setup)
        except AttributeError:
            raise ValueError("Please provide a valid disturbance setup in the config files.")

    def create_action_manager(self):
        try:
            return ActionManager(self.config.action_setup, self.config.action_setup.actions_are_relative)
        except AttributeError:
            raise ValueError("Please provide a valid action setup in the config files.")

    def create_output_manager(self):
        try:
            # Decide whether to create a SequentialOutputManager or ParallelOutputManager
            if self.config.physical_execution:
                return OpcuaOutputManager(self.config.output_setup)
            if self.config.parallel_execution:
                return ParallelOutputManager(self.config.output_setup)
            else:
                return SequentialOutputManager(self.config.output_setup)
        except AttributeError:
            raise ValueError("Please provide a valid output setup in the config files.")

    def create_objective_manager(self):
        try:
            objective = objective_functions_augsburg.baseline_objective \
                if self.config.physical_execution else baseline_objective
            penalty = objective_functions_augsburg.baseline_penalty \
                if self.config.physical_execution else baseline_penalty
            return ObjectiveManager(objective, penalty, self.config.objective_setup)
        except AttributeError:
            raise ValueError("Please provide a valid objective setup in the config files.")

    def create_scenario_manager(self):
        try:
            return ScenarioManager(self.config.scenario_setup)
        except AttributeError:
            raise ValueError("Please provide a valid scenario setup in the config files.")

    def create_experiment_tracker(self):
        try:
            if self.config.tracking_enabled:
                return WandBTracker(self.config.wandb_settings, self.config)
            else:
                return EmptyTracker(OmegaConf.create(), OmegaConf.create())
        except AttributeError:
            raise ValueError("Please provide a valid tracking setup in the config files.")

    def create_environment(self) -> AbstractEnvironment:
        realistic_setting = self.config.get(key="realistic_setting", default_value=False)
        env_class = RealisticEnvironment if realistic_setting else Environment
        return env_class(
            self.config.env_setup,
            self.create_disturbance_manager(),
            self.create_action_manager(),
            self.create_output_manager(),
            self.create_objective_manager(),
            self.create_scenario_manager(),
            self.create_experiment_tracker()
        )
