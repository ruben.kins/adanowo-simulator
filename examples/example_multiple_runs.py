import os

import numpy as np
import hydra
from omegaconf import DictConfig

from adanowo_simulator.environment_factory import EnvironmentFactory
from adanowo_simulator.gym_wrapper import GymWrapper

os.environ["WANDB_SILENT"] = "true"


@hydra.main(version_base=None, config_path="../config", config_name="multiple_runs")
def main(config: DictConfig):
    factory = EnvironmentFactory(config)
    environment = factory.create_environment()
    gym_wrapper = GymWrapper(environment, config.gym_setup, config.action_setup, config.env_setup)
    
    # This loop does "num_experiment_steps" simulations and in every simulation
    # Between every simulation there is a reset where the constraints of the quality_bounds change
    # according the function _update_target in scenario_manager

    # add "group" keyword to the config file for w&b to recognize the runs belong to the same experiment:
    group = "example_multiple_runs"
    config.wandb_settings["group"] = group
    for _ in range(config.num_experiments):
        observations, reward = gym_wrapper.reset()
        #  initialize algorithm
        
        for _ in range(config.num_experiment_steps):
            observations, reward, _, _, _ = gym_wrapper.step(np.random.uniform(
                low=-1, high=1, size=len(config.env_setup.used_setpoints)))
            print(reward)
    gym_wrapper.close()


if __name__ == "__main__":
    main()
