from typing import TypedDict

from numpy.typing import NDArray
from numpy import float64


class Constraint(TypedDict, total=False):
    lower: float
    upper: float


class OptionalConstraint(TypedDict, total=False):
    lower: float | None
    upper: float | None


StateDict = dict[str, float]
StateDictArray = dict[str, NDArray[float64]]
OutputDict = dict[str, float | None]  # can be none if no measurements available
ConstraintCheckDict = dict[str, bool]
ConstraintsDict = dict[str, Constraint | None]
OptionalConstraintsDict = dict[str, OptionalConstraint | None]
LogVarDict = dict[str, dict[str, float] | None]
StateArray = NDArray[float64]
OutputArray = NDArray[float64]  # can be NaN if no measurements available
