import numpy as np
from numpy.typing import NDArray

G_MIN_TO_KG_H = 6 / 100
CARD_WIDTH = 1.0  # m


def prcnt_to_mult(prcnt: float) -> float:
    return (prcnt / 100) + 1


def calculate(X: dict) -> NDArray:
    X = X.copy()
    for key in X.keys():
        X[key] = np.array(X[key]).reshape(-1, 1)

    mass_throughput = \
        X["CardDeliveryWeightPerArea"] * \
        CARD_WIDTH * \
        X["CardDeliverySpeed"] * \
        G_MIN_TO_KG_H

    mass_throughput = np.array(mass_throughput).flatten()
    return mass_throughput
