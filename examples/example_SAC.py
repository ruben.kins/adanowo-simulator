import os

import numpy as np
import hydra
from omegaconf import DictConfig
from stable_baselines3 import SAC
from torch.nn import ReLU

from adanowo_simulator.environment_factory import EnvironmentFactory
from adanowo_simulator.gym_wrapper import GymWrapper

os.environ["WANDB_SILENT"] = "true"


@hydra.main(version_base=None, config_path="../config", config_name="domain_randomization")
def main(config: DictConfig):

    # Create the environment, which is a simulated production process
    factory = EnvironmentFactory(config)
    environment = factory.create_environment()
    gym_wrapper = GymWrapper(environment, config.gym_setup, config.action_setup, config.env_setup)

    # Define the policy, which is the "brain" of the agent and maps from env states to actions
    policy_kwargs = dict(
        net_arch=dict(pi=[512] * 3,  # Actor network architecture
                      qf=[512] * 3),  # Critic network architecture
        activation_fn=ReLU
    )

    # Create the agent# create our Agent, which is a Soft Actor-Critic RL agent
    model = SAC('MlpPolicy', gym_wrapper, verbose=2,
                batch_size=256,
                buffer_size=5000,
                gamma=0.99,  # discount factor: How much does the agent care about future rewards
                learning_rate=0.0003,  # learning rate: How fast does the agent learn
                learning_starts=1000,  # The agent collects random transitions at first to avoid early optimization
                ent_coef='auto_0.01',  # Exploration vs exploitation trade-off
                tau=0.005,
                policy_kwargs=policy_kwargs
                )
    model.learn(total_timesteps=3000)  # Train the agent

    # Test the agent
    obs, _ = gym_wrapper.reset()
    for _ in range(600):  # Run the agent for 600 steps
        action, _states = model.predict(obs, deterministic=True)
        obs, reward, _, _, _ = gym_wrapper.step(action)
        assert np.isnan(obs).sum() == 0

    # clean up after yourself ;)
    gym_wrapper.close()


if __name__ == "__main__":
    main()
