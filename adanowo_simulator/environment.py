import sys
import logging
from copy import copy
from omegaconf import DictConfig, OmegaConf

from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
from adanowo_simulator.abstract_base_classes.output_manager import AbstractOutputManager
from adanowo_simulator.abstract_base_classes.objective_manager import AbstractObjectiveManager
from adanowo_simulator.abstract_base_classes.action_manager import AbstractActionManager
from adanowo_simulator.abstract_base_classes.disturbance_manager import AbstractDisturbanceManager
from adanowo_simulator.abstract_base_classes.experiment_tracker import AbstractExperimentTracker
from adanowo_simulator.abstract_base_classes.scenario_manager import AbstractScenarioManager
from adanowo_simulator.datatypes import StateDict, OutputDict, OptionalConstraintsDict, LogVarDict, ConstraintCheckDict

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


def compile_log_variables(
    objective_value: float,
    setpoints_okay: ConstraintCheckDict,
    dependent_variables_okay: ConstraintCheckDict,
    output_constraints_met: ConstraintCheckDict,
    actions: StateDict,
    outputs: OutputDict,
    setpoints: StateDict,
    dependent_variables: StateDict,
    disturbances: StateDict
) -> LogVarDict:
    log_dict: LogVarDict = {
        "Objective-Value": objective_value,
        "Performance-Metrics": {
            "Setpoint-Constraints-Met": int(all(setpoints_okay.values())),
            "Dependent-Variable-Constraints-Met": int(all(dependent_variables_okay.values())),
            "Output-Constraints-Met": int(all(output_constraints_met.values()))
        },
        "Actions": actions,
        "Output-Constraints-Met": {key: int(value) for key, value in output_constraints_met.items()},
        "Outputs": outputs,
        "Setpoint-Constraints-Met": {key: int(value) for key, value in setpoints_okay.items()},
        "Setpoints": setpoints,
        "Dependent-Variable-Constraints-Met": {key: int(value) for key, value in dependent_variables_okay.items()},
        "Dependent-Variables": dependent_variables,
        "Disturbances": disturbances
    }
    return log_dict


class Environment(AbstractEnvironment):
    def __init__(
            self, config: DictConfig, disturbance_manager: AbstractDisturbanceManager,
            action_manager: AbstractActionManager, output_manager: AbstractOutputManager,
            objective_manager: AbstractObjectiveManager, scenario_manager: AbstractScenarioManager,
            experiment_tracker: AbstractExperimentTracker):
        self._disturbance_manager: AbstractDisturbanceManager = disturbance_manager
        self._action_manager: AbstractActionManager = action_manager
        self._output_manager: AbstractOutputManager = output_manager
        self._objective_manager: AbstractObjectiveManager = objective_manager
        self._scenario_manager: AbstractScenarioManager = scenario_manager
        self._experiment_tracker: AbstractExperimentTracker = experiment_tracker

        self.log_vars: LogVarDict | None = None
        self._initial_config: DictConfig = config.copy()
        self._config: DictConfig = self._initial_config.copy()
        self._step_index: int = -1
        self._ready: bool = False
        self._return_penalty = config.get(key="return_penalty", default_value=True)
        self._return_tentative_dependent = config.get(key="return_tentative_dependent", default_value=False)

        self._safety_violation: bool = False
        logger.info("Environment has been created.")

    @property
    def config(self) -> DictConfig:
        return self._config

    @config.setter
    def config(self, c):
        self._config = c

    @property
    def safety_violation(self) -> bool:
        return self._safety_violation

    @property
    def disturbance_manager(self) -> AbstractDisturbanceManager:
        return self._disturbance_manager

    @property
    def action_manager(self) -> AbstractActionManager:
        return self._action_manager

    @property
    def output_manager(self) -> AbstractOutputManager:
        return self._output_manager

    @property
    def objective_manager(self) -> AbstractObjectiveManager:
        return self._objective_manager

    @property
    def scenario_manager(self) -> AbstractScenarioManager:
        return self._scenario_manager

    @property
    def experiment_tracker(self) -> AbstractExperimentTracker:
        return self._experiment_tracker

    @property
    def step_index(self):
        return self._step_index

    def step(self, actions: dict) -> tuple[float, StateDict, OutputDict, OptionalConstraintsDict]:
        if not self._ready:
            raise RuntimeError("Cannot call step() before calling reset().")
        try:
            if self._step_index == 1:
                logger.info("Experiment is running.")
            assert set(actions.keys()) == set(self._config.used_setpoints), "Action dict does not match used setpoints."
            disturbances = self._disturbance_manager.step()
            setpoints, dependent_variables, tentative_dependent, setpoints_okay, dependent_variables_okay = \
                self._action_manager.step(actions, disturbances)
            state = disturbances | setpoints | dependent_variables
            outputs = self._output_manager.step(state)
            self._check_safety_violation(setpoints_okay, dependent_variables_okay, outputs)

            f_aug, f_obj, output_constraints_met = self._objective_manager.step(
                state, outputs, self._safety_violation
            )
            log_variables = compile_log_variables(
                f_aug,
                setpoints_okay,
                dependent_variables_okay,
                output_constraints_met,
                actions,
                outputs,
                setpoints,
                dependent_variables,
                disturbances
            )
            self._experiment_tracker.step(log_variables, self._step_index)
            self.log_vars = copy(log_variables)

            # Execute scenario for the next step so the agent is already informed about production context changes.
            if self._return_tentative_dependent:
                state_with_new_context, quality_bounds_next = self._prepare_next_step(setpoints, tentative_dependent)
            else:
                state_with_new_context, quality_bounds_next = self._prepare_next_step(setpoints, dependent_variables)

        except Exception as e:
            self.close()
            raise e

        if self._return_penalty:
            objective_value = f_aug
        else:
            objective_value = f_obj
        return objective_value, state_with_new_context, outputs, quality_bounds_next

    def reset(self) -> tuple[float, StateDict, OutputDict, OptionalConstraintsDict]:
        logger.info("Resetting environment...")
        try:
            # step 0.
            self._step_index = 0
            self._config = self._initial_config.copy()
            self._scenario_manager.reset()
            disturbances = self._disturbance_manager.reset()
            setpoints, dependent_variables, _, setpoints_okay, dependent_variables_okay = \
                self._action_manager.reset(disturbances)
            state = disturbances | setpoints | dependent_variables
            outputs = self._output_manager.reset(state)
            self._check_safety_violation(setpoints_okay, dependent_variables_okay, outputs)
            f_aug, f_obj, output_constraints_met = self._objective_manager.reset(
                state, outputs, self._safety_violation)
            log_variables = compile_log_variables(
                f_aug,
                setpoints_okay,
                dependent_variables_okay,
                output_constraints_met,
                {},
                outputs,
                setpoints,
                dependent_variables,
                disturbances
            )
            self._experiment_tracker.reset(log_variables)
            self.log_vars = copy(log_variables)

            # prepare step 1.
            state_with_new_context, quality_bounds_next = self._prepare_next_step(setpoints, dependent_variables)

        except Exception as e:
            self.close()
            raise e

        self._ready = True
        logger.info("...environment has been reset.")

        if self._return_penalty:
            objective_value = f_aug
        else:
            objective_value = f_obj

        return objective_value, state_with_new_context, outputs, quality_bounds_next

    def close(self) -> None:
        logger.info("Closing environment...")
        exceptions = []
        try:
            self._disturbance_manager.close()
        except Exception as e:
            exceptions.append(e)
        try:
            self._action_manager.close()
        except Exception as e:
            exceptions.append(e)
        try:
            self._output_manager.close()
        except Exception as e:
            exceptions.append(e)
        try:
            self._objective_manager.close()
        except Exception as e:
            exceptions.append(e)
        try:
            self._scenario_manager.close()
        except Exception as e:
            exceptions.append(e)
        try:
            self._experiment_tracker.close()
        except Exception as e:
            exceptions.append(e)
        self._step_index = -1
        self._ready = False
        if exceptions:
            raise Exception(exceptions)
        logger.info("...environment has been closed.")

    def _prepare_next_step(self, setpoints: StateDict, dependent_variables: StateDict) -> \
            tuple[StateDict, OptionalConstraintsDict]:
        self._step_index += 1
        self._scenario_manager.step(self._step_index, self._disturbance_manager, self._output_manager,
                                    self._objective_manager)
        disturbances = self._disturbance_manager.step()
        state_with_new_context = disturbances | setpoints | dependent_variables
        quality_bounds_next = OmegaConf.to_container(self._objective_manager.config.output_bounds)
        return state_with_new_context, quality_bounds_next

    def _check_safety_violation(self, setpoints_okay, dependent_variables_okay,
                                outputs: OutputDict = None, overwrite_when_valid: bool = True) -> None:
        if all((setpoints_okay | dependent_variables_okay).values()):
            if outputs is not None and overwrite_when_valid:
                outputs["UserFeedback"] = float(self._config.no_violation_state_value)
            elif not overwrite_when_valid and not "UserFeedback" in outputs:
                outputs["UserFeedback"] = None
            self._safety_violation = False
        else:
            if outputs is not None:
                outputs["UserFeedback"] = float(self._config.safety_violation_state_value)
            self._safety_violation = True
