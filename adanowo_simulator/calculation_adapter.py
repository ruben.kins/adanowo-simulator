from types import ModuleType, MethodType
import numpy as np

from adanowo_simulator.abstract_base_classes.calculation_adapter import AbstractCalculationAdapter
from adanowo_simulator.datatypes import StateDict, StateArray


class CalculationAdapter(AbstractCalculationAdapter):

    def __init__(self, calculation_module: ModuleType) -> None:
        self._calculate: MethodType = calculation_module.calculate

    def calculate(self, X: StateDict) -> StateArray:
        c = np.array(self._calculate(X)).flatten()
        return c
