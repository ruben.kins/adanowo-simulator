import wandb as wb
from omegaconf import DictConfig, OmegaConf

from adanowo_simulator.abstract_base_classes.experiment_tracker import AbstractExperimentTracker
from adanowo_simulator.datatypes import LogVarDict


class WandBTracker(AbstractExperimentTracker):

    def __init__(self, tracker_config: DictConfig, tracked_config: DictConfig):
        self._initial_tracker_config: DictConfig = tracker_config.copy()
        self._tracker_config: DictConfig = self._initial_tracker_config.copy()
        self._tracked_config: DictConfig = tracked_config.copy()
        self._run = None
        self._ready: bool = False

    @property
    def config(self) -> DictConfig:
        return self._tracker_config

    @config.setter
    def config(self, c):
        self._tracker_config = c

    def step(self, log_variables: LogVarDict, step_index: int) -> None:
        if self._ready:
            try:
                self._run.log(
                    self._flatten_dict(log_variables),
                    step=step_index,
                    commit=True
                )
            except Exception as e:
                self.close()
                raise e
        else:
            raise RuntimeError("Cannot call step() before calling reset().")

    def reset(self, initial_log_variables: LogVarDict) -> None:
        self.close()
        try:
            self._tracker_config = self._initial_tracker_config.copy()
            tracked_config_container = OmegaConf.to_container(self._tracked_config)
            self._run = wb.init(config=tracked_config_container, reinit=True, **self._tracker_config)
            wb.define_metric("Objective-Value", summary="mean")
            self._ready = True
        except Exception as e:
            self.close()
            raise e

    def close(self) -> None:
        if self._run:
            self._run.finish()
            self._tracker_config = OmegaConf.create()
            self._run = None
            self._ready = False

    def _flatten_dict(self, nested_dict, parent_key='', sep='/'):
        items = []
        for k, v in nested_dict.items():
            new_key = f'{parent_key}{sep}{k}' if parent_key else k
            if isinstance(v, dict):
                items.extend(self._flatten_dict(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)


class EmptyTracker(AbstractExperimentTracker):

    def __init__(self, tracker_config: DictConfig, tracked_config: DictConfig):
        self._initial_tracker_config: DictConfig = tracker_config.copy()
        self._tracker_config: DictConfig = self._initial_tracker_config.copy()
        self._tracked_config: DictConfig = tracked_config.copy()
        self._run = None
        self._ready: bool = False

    @property
    def config(self) -> DictConfig:
        return self._tracker_config

    @config.setter
    def config(self, c):
        self._tracker_config = c

    def step(self, log_variables: LogVarDict, step_index: int) -> None:
        return

    def reset(self, initial_log_variables: LogVarDict) -> None:
        return

    def close(self) -> None:
        return
