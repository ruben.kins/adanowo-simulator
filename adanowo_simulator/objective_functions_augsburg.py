from omegaconf import DictConfig

from adanowo_simulator.transformations import ParameterizedStandardScaler
from adanowo_simulator.datatypes import StateDict, OutputDict

H_TO_MIN = 60


def baseline_objective(state: StateDict, outputs: OutputDict, config: DictConfig) -> float:
    # material costs
    material_costs = state["MassThroughput"] * config.fibre_costs
    # energy costs
    if outputs["LinePowerConsumption"] is None:
        energy_costs = 0.0
    else:
        energy_costs = outputs["LinePowerConsumption"] * config.energy_costs
    # Production income
    income = config.selling_price * state["ProductionSpeed"] * H_TO_MIN * state["ProductWidth"]

    # Component 1: Calculate economic efficiency
    contribution_margin = income - energy_costs - material_costs

    # Component 2: card floor evenness
    scaler = ParameterizedStandardScaler(config.unevenness_signal.mean, config.unevenness_signal.std)
    if outputs["NonwovenUnevenness"] is None:
        scaled_signal = 0.0
    else:
        scaled_signal = scaler.transform(float(outputs["NonwovenUnevenness"]))
    floor_quality = scaled_signal * config.floor_quality_weight

    reward = contribution_margin - floor_quality

    return reward


def baseline_penalty(state: StateDict, outputs: OutputDict, config: DictConfig) -> float:
    # material costs
    material_costs = state["MassThroughput"] * config.fibre_costs
    # energy costs
    if outputs["LinePowerConsumption"] is None:
        energy_costs = 0.0
    else:
        energy_costs = outputs["LinePowerConsumption"] * config.energy_costs

    # Calculate economic loss
    penalty = - energy_costs - material_costs

    return penalty
