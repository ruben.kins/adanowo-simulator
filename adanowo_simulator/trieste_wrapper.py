import tensorflow as tf
import numpy as np
from omegaconf import DictConfig, OmegaConf

from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
from adanowo_simulator import transformations
from adanowo_simulator.datatypes import OutputDict, StateDict, OptionalConstraintsDict, ConstraintsDict


class TriesteWrapper:
    def __init__(self, environment: AbstractEnvironment, config: DictConfig, action_config: DictConfig,
                 env_config: DictConfig):
        self._environment: AbstractEnvironment = environment
        self._config = config.copy()
        self._action_bounds_dict = OmegaConf.to_container(action_config.setpoint_bounds)
        self._setpoints_list = list(env_config.used_setpoints)
        self._dependent_variables_list = list(env_config.used_dependent_variables)
        self._dependent_bounds_dict = OmegaConf.to_container(action_config.dependent_variable_bounds)
        self._outputs_list = list(env_config.used_outputs)
        
    def step(self, actions_tf: tf.Tensor) -> tuple[tf.Tensor, tf.Tensor, tf.Tensor]:
        
        # Transform Tensorflow to np.array
        actions_array = np.array(actions_tf[:actions_tf.shape[0]]).flatten()

        # transform actions
        action = transformations.array_to_dict(actions_array, self._setpoints_list,
                                               bounds_for_scaling=self._action_bounds_dict, mode="min_max")
         
        # perform step. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.step(action)

        constraint_value_dict, constraint_bounds, output_ranges = self._compile_constraints(
            state, outputs, quality_bounds)
        
        constraints_zeroed = self._transform_constraints(constraint_value_dict, constraint_bounds, output_ranges)

        # Transformation into tensorflow
        reward_tensor, outputs_tensor = self.transform_tensorflow(reward, constraint_value_dict)
        
        user_feedback = outputs["UserFeedback"]

        return reward_tensor, constraints_zeroed, user_feedback

    def reset(self) -> tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        
        # perform reset. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.reset()
        
        state_array = transformations.dict_to_array(state, self._setpoints_list, mode="no_transformation")

        # Convert NumPy array to TensorFlow tensor and reshape to (1, 1)
        state_tensor = tf.convert_to_tensor(state_array.reshape(-1, 1), dtype=tf.float64)
        state_tensor = tf.expand_dims(state_tensor, axis=-1)   

        constraint_value_dict, constraint_bounds, output_ranges = self._compile_constraints(
            state, outputs, quality_bounds)
        
        constraints_zeroed = self._transform_constraints(constraint_value_dict, constraint_bounds, output_ranges)
        
        # Transformation into tensorflow
        reward_tensor, outputs_tensor = self.transform_tensorflow(reward, constraint_value_dict)    
        
        user_feedback = outputs["UserFeedback"]

        return state_tensor, reward_tensor, constraints_zeroed, user_feedback

    def close(self) -> None:
        self._environment.close()

    @staticmethod
    def transform_tensorflow(reward: float, output: OutputDict) -> tuple[tf.Tensor, tf.Tensor]:
        
        # Extract data from the output dictionary and convert to list
        output_list = list(output.values())
        
        # Append reward
        output_list.insert(0, reward)

        # Convert list to TensorFlow tensor and reshape it
        tf_tensor = tf.convert_to_tensor(np.array(output_list).reshape(-1, 1), dtype=tf.float64)
        
        # Reshape the tensor to have a shape of (1, 1)
        tf_reshaped_tensor = tf.expand_dims(tf_tensor, axis=-1)     

        return tf_reshaped_tensor[0], tf_reshaped_tensor[1:]

    def _compile_constraints(self, state: StateDict, outputs: OutputDict, quality_bounds: OptionalConstraintsDict) \
            -> tuple[OutputDict, OptionalConstraintsDict, ConstraintsDict]:
        dependent_variable_dict = {key: state[key] for key in self._dependent_variables_list}
        constraint_value_dict = {**outputs, **dependent_variable_dict}
        constraint_bounds = {**quality_bounds, **self._dependent_bounds_dict}
        output_ranges = OmegaConf.to_container(self._config.output_range)
        return constraint_value_dict, constraint_bounds, output_ranges

    def _transform_constraints(self, outputs: OutputDict, bounds: OptionalConstraintsDict, ranges: ConstraintsDict
                               ) -> list[tf.Tensor | None]:
        """
        Transforms constraints with upper and lower bounds into a format
        compatible with the optimizer's one-sided constraint expectation.

        Parameters:
        - constraints_output: list[tf.Tensor | None], output from the evaluation function.
        - bounds: list[dict], each dict containing 'upper' and 'lower' bounds.

        Returns:
        - transformed_constraints: list[tf.Tensor], transformed constraints.
        """
        transformed_constraints: list[tf.Tensor | None] = []
        constraint_list = self._outputs_list + self._dependent_variables_list
        bounds_list = [bounds[key] if key in bounds else {"lower": None, "upper": None}
                       for key in constraint_list]
        outputs_list = [outputs[key] for key in constraint_list]
        ranges_list = [ranges[key] if key in ranges else {"lower": None, "upper": None}
                       for key in constraint_list]

        for output, bound, rg in zip(outputs_list, bounds_list, ranges_list):
            if output is None:
                if bound["lower"] is not None:
                    transformed_constraints.append(None)
                    
                if bound["upper"] is not None:
                    transformed_constraints.append(None)
                    
                continue

            lower_bound = bound['lower']
            if lower_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([lower_bound - output]), rg["lower"], rg["upper"]
                )
                constraint = tf.convert_to_tensor(transformed_value, dtype=tf.float64)
                transformed_constraints.append(tf.reshape(constraint, (1, -1)))
            upper_bound = bound['upper']
            if upper_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([output - upper_bound]), rg["lower"], rg["upper"]
                )
                constraint = tf.convert_to_tensor(transformed_value, dtype=tf.float64)
                transformed_constraints.append(tf.reshape(constraint, (1, -1)))

        return transformed_constraints
