import numpy as np
from numpy.typing import NDArray
from omegaconf import DictConfig
from omegaconf import OmegaConf

from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
from adanowo_simulator import transformations
from adanowo_simulator.datatypes import OutputDict, OptionalConstraintsDict, ConstraintsDict, StateDict

# for this implementation, with some adjustments: https://github.com/kirschnj/LineBO


class LineBOWrapper:
    def __init__(self, environment: AbstractEnvironment, config: DictConfig, action_config: DictConfig,
                 env_config: DictConfig):
        self._environment: AbstractEnvironment = environment
        self._config = config.copy()
        self._action_bounds_dict = OmegaConf.to_container(action_config.setpoint_bounds)
        self._setpoints_list = list(env_config.used_setpoints)
        self._outputs_list = list(env_config.used_outputs)
        self._dependent_variables_list = list(env_config.used_dependent_variables)
        self._dependent_bounds_dict = OmegaConf.to_container(action_config.dependent_variable_bounds)

    def step(self, actions_array: NDArray) -> dict[str, NDArray | list[NDArray] | bool]:
        data = {}
        actions_array = np.squeeze(actions_array)
        actions_rescaled = transformations.array_to_dict(actions_array, self._setpoints_list,
                                                         bounds_for_scaling=self._action_bounds_dict,
                                                         mode="min_max")

        # perform step. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.step(actions_rescaled)

        data = self._transform_env_output(reward, state, outputs, quality_bounds)

        return data

    def reset(self) -> dict[str, NDArray | list[NDArray] | bool]:
        # perform reset. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.reset()

        data = self._transform_env_output(reward, state, outputs, quality_bounds)

        return data

    def close(self) -> None:
        self._environment.close()

    def _transform_env_output(self, reward: float, state: StateDict, outputs: OutputDict,
                              quality_bounds: OptionalConstraintsDict) -> dict[
                              str, NDArray | list[NDArray] | bool]:
        data = {}
        if reward is None:
            reward = float('NaN')

        setpoint_states = {key: state[key] for key in self._setpoints_list}
        setpoint_array = transformations.dict_to_array(setpoint_states, self._setpoints_list,
                                                       self._action_bounds_dict, mode="min_max")
        setpoint_array = np.squeeze(setpoint_array)

        reward_array = np.array([reward / float(self._config.max_reward)])

        constraint_value_dict, constraint_bounds, output_ranges = self._compile_constraints(
            state, outputs, quality_bounds)

        data["update_constraints"] = outputs["UserFeedback"] != self._environment.config.safety_violation_state_value

        constraints_zeroed = self._transform_constraints(constraint_value_dict, constraint_bounds, output_ranges)

        data["y"] = reward_array
        data["x"] = setpoint_array
        data["s"] = constraints_zeroed

        return data

    def _compile_constraints(self, state: StateDict, outputs: OutputDict, quality_bounds: OptionalConstraintsDict) \
            -> tuple[OutputDict, OptionalConstraintsDict, ConstraintsDict]:
        dependent_variable_dict = {key: state[key] for key in self._dependent_variables_list}
        constraint_value_dict = {**outputs, **dependent_variable_dict}
        constraint_bounds = {**quality_bounds, **self._dependent_bounds_dict}
        output_ranges = OmegaConf.to_container(self._config.output_range)
        return constraint_value_dict, constraint_bounds, output_ranges

    def _transform_constraints(self, outputs: OutputDict, bounds: OptionalConstraintsDict, ranges: ConstraintsDict
                               ) -> list[NDArray | None]:
        """
        Transforms constraints with upper and lower bounds into a format
        compatible with the optimizer's one-sided constraint expectation.

        Parameters:
        - constraints_output: list[torch.Tensor | None], output from the evaluation function.
        - bounds: list[dict], each dict containing 'upper' and 'lower' bounds.

        Returns:
        - transformed_constraints: list[torch.Tensor], transformed constraints.
        """
        transformed_constraints: list[NDArray | None] = []
        constraint_list = self._outputs_list + self._dependent_variables_list
        bounds_list = [bounds[key] if key in bounds else {"lower": None, "upper": None}
                       for key in constraint_list]
        outputs_list = [outputs[key] for key in constraint_list]
        ranges_list = [ranges[key] if key in ranges else {"lower": None, "upper": None}
                       for key in constraint_list]

        for output, bound, rg in zip(outputs_list, bounds_list, ranges_list):
            if output is None:
                if bound["lower"] is not None:
                    transformed_constraints.append(np.array([float('NaN')]).reshape(1, -1))
                if bound["upper"] is not None:
                    transformed_constraints.append(np.array([float('NaN')]).reshape(1, -1))
                continue

            lower_bound = bound['lower']
            if lower_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([lower_bound - output]), rg["lower"], rg["upper"]
                )/2
                constraint = np.squeeze(transformed_value)
                transformed_constraints.append(constraint)
            upper_bound = bound['upper']
            if upper_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([output - upper_bound]), rg["lower"], rg["upper"]
                )
                constraint = np.squeeze(transformed_value)
                transformed_constraints.append(constraint)

        return transformed_constraints
