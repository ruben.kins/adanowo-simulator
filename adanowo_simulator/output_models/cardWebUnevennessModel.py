import gpytorch
import numpy as np
from gpytorch.constraints import GreaterThan
from gpytorch.kernels import PolynomialKernel, RBFKernel, ScaleKernel


def rescale(data, before_min, before_max, after_min, after_max):
    # Convert the input data to a numpy array if it isn't already one
    data = np.array(data)

    # Scale the data
    scaled = (data - before_min) / (before_max - before_min)
    rescaled = scaled * (after_max - after_min) + after_min

    return rescaled.reshape(-1, 1)


def unpack_dict(X: dict, training_features: list[str]) -> np.array:
    X = X.copy()
    for key in X.keys():
        X[key] = np.array(X[key]).reshape(-1, 1)
    X_unpacked = []
    for f in training_features:
        if f == "FG_soll":
            rescaled = rescale(X["CardDeliveryWeightPerArea"],
                               41.0, 77.28, 7.88, 22.69)
            X_unpacked.append(rescaled)
        elif f == "Durchsatz":
            rescaled = rescale(X["MassThroughput"],
                               300.0, 1201.0, 6.0, 65.4)
            X_unpacked.append(rescaled)
        elif f == "v_Hauptwalze":
            X_unpacked.append(X["v_MainCylinder"])
        elif f == "v_Arbeiter_HW":
            X_unpacked.append(X["v_WorkerMain"])
        elif f == "v_Wender_HW":
            X_unpacked.append(X["v_StripperMain"])
    return np.concatenate(X_unpacked, axis=1)


likelihood = gpytorch.likelihoods.GaussianLikelihood()


class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood_mdl):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood_mdl)

        kernel = ScaleKernel(RBFKernel(ard_num_dims=5, lengthscale_constraint=GreaterThan(0.5)))

        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = kernel

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)
