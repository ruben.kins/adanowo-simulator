import numpy as np
import torch
from omegaconf import DictConfig
from omegaconf import OmegaConf
from botorch.utils.transforms import unnormalize, normalize

from adanowo_simulator.abstract_base_classes.environment import AbstractEnvironment
from adanowo_simulator import transformations
from adanowo_simulator.datatypes import OutputDict, OptionalConstraintsDict, ConstraintsDict, StateDict

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
dtype = torch.double
tkwargs = {"device": device, "dtype": dtype}

# for this implementation https://botorch.org/tutorials/scalable_constrained_bo


class SCBOWrapper:
    def __init__(self, environment: AbstractEnvironment, config: DictConfig, action_config: DictConfig,
                 env_config: DictConfig):
        self._environment: AbstractEnvironment = environment
        self._config = config.copy()
        self._action_bounds_dict = OmegaConf.to_container(action_config.setpoint_bounds)
        self._setpoints_list = list(env_config.used_setpoints)
        self._outputs_list = list(env_config.used_outputs)
        self._dependent_variables_list = list(env_config.used_dependent_variables)
        self._dependent_bounds_dict = OmegaConf.to_container(action_config.dependent_variable_bounds)

    def step(self, actions_tensor: torch.Tensor) -> tuple[torch.Tensor, torch.Tensor, list[torch.Tensor],
                                                          list[torch.Tensor], bool]:
        bounds = self.constraint_dict_to_tensor(self._action_bounds_dict, self._setpoints_list)
        actions_tensor = unnormalize(actions_tensor, bounds)
        # transform actions
        actions_array = actions_tensor.cpu().detach().numpy().squeeze()

        action = transformations.array_to_dict(actions_array, self._setpoints_list, mode="no_transformation")

        # perform step. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.step(action)
        state_tensor, reward_tensor, safety_zeroed, quality_zeroed, experiment_aborted = (
            self._transform_env_output(reward, state, outputs, quality_bounds)
        )
        return state_tensor, reward_tensor, safety_zeroed, quality_zeroed, experiment_aborted

    def reset(self) -> tuple[torch.Tensor, torch.Tensor, list[torch.Tensor | None], list[torch.Tensor | None], bool]:
        # perform reset. Be aware that the outputs can contain None values.
        reward, state, outputs, quality_bounds = self._environment.reset()
        state_tensor, reward_tensor, safety_zeroed, quality_zeroed, experiment_aborted = (
            self._transform_env_output(reward, state, outputs, quality_bounds)
        )
        return state_tensor, reward_tensor, safety_zeroed, quality_zeroed, experiment_aborted

    def close(self) -> None:
        self._environment.close()

    def _transform_env_output(self, reward: float, state: StateDict, outputs: OutputDict,
                              quality_bounds: OptionalConstraintsDict) -> (
            tuple)[torch.Tensor, torch.Tensor, list[torch.Tensor | None], list[torch.Tensor | None], bool]:
        if reward is None:
            reward = float('NaN')
        # be aware that context is not returned
        state_tensor = self._transform_state(state)

        reward_tensor = torch.tensor([reward / float(self._config.max_reward)], **tkwargs).reshape(1, -1)

        dependent_value_dict, dependent_bounds, output_value_dict, output_bounds, ranges = self._compile_constraints(
            state, outputs, quality_bounds)

        experiment_aborted = outputs["UserFeedback"] == self._environment.config.safety_violation_state_value
        safety_zeroed = self._transform_constraints(dependent_value_dict, dependent_bounds, ranges,
                                                    self._dependent_variables_list)
        quality_zeroed = self._transform_constraints(output_value_dict, output_bounds, ranges, self._outputs_list)

        # add user feedback to quality values
        quality_zeroed.append(
            torch.tensor([0.4 if outputs["UserFeedback"] == 3 else -1], **tkwargs).unsqueeze(0)
        )
        quality_zeroed.append(
            torch.tensor([0.4 if outputs["UserFeedback"] == 4 else -1], **tkwargs).unsqueeze(0)
        )
        if outputs["UserFeedback"] in [3, 4]:
            reward_tensor = torch.tensor([float('NaN')], **tkwargs).reshape(1, -1)

        return state_tensor, reward_tensor, safety_zeroed, quality_zeroed, experiment_aborted

    def _transform_state(self, state: StateDict) -> torch.Tensor:
        state_array = transformations.dict_to_array(state, self._setpoints_list, mode="no_transformation")
        state_tensor = torch.tensor(state_array, **tkwargs).reshape(1, -1)

        state_bounds = self.constraint_dict_to_tensor(self._action_bounds_dict, self._setpoints_list)
        state_tensor = normalize(state_tensor, state_bounds)
        return state_tensor

    def _compile_constraints(self, state: StateDict, outputs: OutputDict, quality_bounds: OptionalConstraintsDict) \
            -> tuple[OutputDict, OptionalConstraintsDict, OutputDict, OptionalConstraintsDict, ConstraintsDict]:
        dependent_variable_dict = {key: state[key] for key in self._dependent_variables_list}

        output_ranges = OmegaConf.to_container(self._config.output_range)

        return dependent_variable_dict, self._dependent_bounds_dict, outputs, quality_bounds, output_ranges

    @staticmethod
    def _transform_constraints(outputs: OutputDict, bounds: OptionalConstraintsDict, ranges: ConstraintsDict,
                               order_list: list[str]) -> list[torch.Tensor]:
        """
        Transforms constraints with upper and lower bounds into a format
        compatible with the optimizer's one-sided constraint expectation.

        Parameters:
        - constraints_output: list[torch.Tensor | None], output from the evaluation function.
        - bounds: list[dict], each dict containing 'upper' and 'lower' bounds.

        Returns:
        - transformed_constraints: list[torch.Tensor], transformed constraints.
        """
        transformed_constraints: list[torch.Tensor] = []
        bounds_list = [bounds[key] if key in bounds else {"lower": None, "upper": None}
                       for key in order_list]
        outputs_list = [outputs[key] for key in order_list]
        ranges_list = [ranges[key] if key in ranges else {"lower": None, "upper": None}
                       for key in order_list]

        for output, bound, rg in zip(outputs_list, bounds_list, ranges_list):
            if output is None:
                if bound["lower"] is not None:
                    transformed_constraints.append(torch.tensor([float('NaN')], **tkwargs).reshape(1, -1))
                if bound["upper"] is not None:
                    transformed_constraints.append(torch.tensor([float('NaN')], **tkwargs).reshape(1, -1))
                continue

            lower_bound = bound['lower']
            if lower_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([lower_bound - output]), rg["lower"], rg["upper"]
                )/2
                constraint = torch.tensor(transformed_value, **tkwargs).reshape(1, -1)
                transformed_constraints.append(constraint)
            upper_bound = bound['upper']
            if upper_bound is not None:
                transformed_value = transformations.div_by_range(
                    np.array([output - upper_bound]), rg["lower"], rg["upper"]
                )/2
                constraint = torch.tensor(transformed_value, **tkwargs).reshape(1, -1)
                transformed_constraints.append(constraint)

        return transformed_constraints

    @staticmethod
    def constraint_dict_to_tensor(constraints: ConstraintsDict, order: list[str]) -> torch.Tensor:
        bounds = torch.zeros(2, len(order), **tkwargs)
        lower_bounds = [constraints[key]["lower"] for key in order]
        upper_bounds = [constraints[key]["upper"] for key in order]
        bounds[0, :] = torch.tensor(lower_bounds, **tkwargs)
        bounds[1, :] = torch.tensor(upper_bounds, **tkwargs)
        return bounds
